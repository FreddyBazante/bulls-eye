//
//  ViewController.swift
//  Bull's Eye
//
//  Created by Freddy Bazante on 13/11/17.
//  Copyright © 2017 Freddy Bazante. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var round: UILabel!
    @IBOutlet weak var slider: UISlider!
    
    //MARK:- ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

     //MARK:- Actions
    
    @IBAction func restartButtonPressed(_ sender: Any) {
    }
    
    @IBAction func playButtonPressed(_ sender: Any) {
    }
    
}

